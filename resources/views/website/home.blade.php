<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
	
	
<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	
</head>

<body>
	
	<div>
		<img src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,h_50/v1582807680/pixel_penguin_logos/new/pixel_penguin_without_creative_solutions-01_xaejgv.png" />
		
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			@csrf
		</form>
		<a href="javascript:void" onclick="$('#logout-form').submit();">
			Logout
		</a>
	</div>
	
	<!-- A grey horizontal navbar that becomes vertical on small screens -->
	<nav class="navbar navbar-expand-sm bg-light">

	  <!-- Links -->
	  <ul class="navbar-nav">
		
		
		  
		@foreach($pages as $page)
		<li class="nav-item">
		  <a class="nav-link" href="/page/{{ $page->id }}">{{ $page->name }}</a>
		</li>
		@endforeach
		<li class="nav-item">
		  <a class="nav-link" href="/contact-us">Contact Us</a>
		</li>
		
		<li class="nav-item">
		  <a class="nav-link" href="/admin/specials">Add/Edit/Remove Specials</a>
		</li>
		  
	  </ul>

	</nav>
	
	<div class="row">
		@foreach($specials as $special)		
		<div class="col-md-3">
			<div style="text-align: center">
				<h4>{{ $special->name }}</h4>
				<p>Was: ${{ $special->was_price }}</p>
				<p>Now: ${{ $special->current_price }}</p>
				<a href="/special/{{ $special->id }}">Click here to view special</a>	
			</div>					
		</div>		
		@endforeach
	</div>
	
	<div>
		{{ $pageDetail->description }}
	</div>
	
	
	
</body>
</html>