<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
	
	
<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<link rel="stylesheet" href="/css/website.css">	
	

	
</head>

<body>
	<div id="app">
		<div>
			<img src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,h_50/v1582807680/pixel_penguin_logos/new/pixel_penguin_without_creative_solutions-01_xaejgv.png" />
		</div>

		<!-- A grey horizontal navbar that becomes vertical on small screens -->
		<nav class="navbar navbar-expand-sm bg-light">

		  <!-- Links -->
		  <ul class="navbar-nav">



			@foreach($pages as $page)
			<li class="nav-item">
			  <a class="nav-link" href="/page/{{ $page->id }}">{{ $page->name }}</a>
			</li>
			@endforeach
			<li class="nav-item">
			  <a class="nav-link" href="/contact-us">Contact Us</a>
			</li>


		  </ul>

		</nav>

		<contact-us-form></contact-us-form>

		<!--
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<form method="post" action="/contact-us/sendmessage" class="was-validated">

					  @if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					  @endif

					  @if(isset($successMessage))	
						<div class="alert alert-success">
							<p>{{ $successMessage }}</p>
						</div>
					  @endif	

					  @csrf

					  <div class="form-group">
						<label for="uname">Name:</label>
						<input type="text" class="form-control" id="uname" placeholder="Enter your name" name="name" >
					  </div>

					  <div class="form-group">
						<label for="uname">Email:</label>
						<input type="text" class="form-control" id="uname" placeholder="Enter your email" name="email" >
					  </div>

					  <div class="form-group">
						<label for="uname">Message:</label>
						<textarea class="form-control" name="message" placeholder="Please enter you message here" ></textarea>
					  </div>

					  </div>
					  <button type="submit" class="btn btn-primary">Submit</button>
					</form>

				</div>



			</div>

		</div>
		-->
	
	</div>
	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		
	<script src="/js/website.js"></script>
</body>
</html>