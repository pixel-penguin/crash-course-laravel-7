<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Upload Image</title>
</head>

<body>
	
	<?php
		$imageName = 'gerritsbigfile';
	?>
	
	<div>
		<img src="https://res.cloudinary.com/crash-course-laravel-7/image/upload/v1/{{ $imageName }}.png" />
		<img src="https://res.cloudinary.com/crash-course-laravel-7/image/upload/c_fill,h_100,w_100/v1/{{ $imageName }}.jpg" />
		<img src="https://res.cloudinary.com/crash-course-laravel-7/image/upload/c_fill,g_face:center,h_100,w_100/v1/{{ $imageName }}.jpg" />
	</div>
	
	<form method="post" action="/upload-image/upload" enctype="multipart/form-data">
		
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		 @csrf
		
		<input name="image_name" type="file" accept="image" />
		
		<button type="submit">Add Image </button>
	</form>
</body>
</html>