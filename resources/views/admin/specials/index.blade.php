<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
	
	
<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	
</head>

<body>
	
	
	<!-- A grey horizontal navbar that becomes vertical on small screens -->
	<nav class="navbar navbar-expand-sm bg-light">

	  <!-- Links -->
	  <ul class="navbar-nav">
		
		<li class="nav-item">
		  <a class="nav-link" href="/">Go Back</a>
		</li>
		  
	  </ul>

	</nav>
	
	<div>
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Was</th>
					<th>New</th>
					<th>Brand</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				
				@foreach($specials as $special)
				<tr>
					<td>{{ $special->name }}</td>
					<td>{{ $special->description }}</td>
					<td>${{ $special->was_price }}</td>
					<td>${{ $special->current_price }}</td>
					<td>{{ $special->brand }}</td>
					<td>
						<a href="/admin/specials/{{ $special->id }}/edit" class="btn btn-sm btn-primary">Edit</a>
						
						<form method="post" action="/admin/specials/{{ $special->id }}">
							@method('delete')
							@csrf
							
							<button class="btn btn-sm btn-danger" type="submit">Delete</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		
		<a href="/admin/specials/create" class="btn btn-primary">Add new Special</a>
	</div>
	
	
	
</body>
</html>