<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;


class TestController extends Controller
{
    public function test1(){
		
		$page = Page::where('id', 2)->first();
		
	
		return view('website.test1', ['page' => $page]);
	}
}
