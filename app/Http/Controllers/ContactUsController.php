<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;

use Mail;

class ContactUsController extends Controller
{
    public function index(){
		
		$pages = Page::All();
		
		return view('website.contact', ['pages' => $pages]);
	}
	
	public function sendMessage(Request $request){
		
		$input = $request->all();
		
		$pages = Page::All();
		
		$validator = $request->validate([
			'name' => 'required',
			'email' => 'required|email',
		]);
		
		
		
		Mail::send('mails.contactus', ['nameInput' => $input['name'], 'messageInput' => $input['message']], function($m){
			$m->subject($subject);
			$m->from('no-reply@pixel-penguin.com', 'PixelPenguin Website');
			
			$m->to('gerrit@pixel-penguin.com');
		});
		
		
		return view('website.contact', ['pages' => $pages])->with('successMessage', 'Thank you! your message has been sent!') ;
			
		
	}

	public function sendMessageAjax(Request $request){
		$input = $request->all();

		//dd($input);

		$validator = $request->validate([
			'name' => 'required',
			'email' => 'required|email',
		]);
		
		
		
		Mail::send('mails.contactus', ['nameInput' => $input['name'], 'messageInput' => $input['message']], function($m){
			$m->from('no-reply@pixel-penguin.com', 'PixelPenguin Website');
			
			$m->to('gerrit@pixel-penguin.com');
		});
		
		return [
			'success' => true,
			'message' => 'Thank you, we will get back to you as soons as possible'
		];
	}
}
